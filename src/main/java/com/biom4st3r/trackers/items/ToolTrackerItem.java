package com.biom4st3r.trackers.items;

import java.util.List;

import net.minecraft.block.Block;
import net.minecraft.block.Blocks;
import net.minecraft.client.item.TooltipContext;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.Formatting;
import net.minecraft.world.World;

import com.biom4st3r.trackers.ModInit;
import com.biom4st3r.trackers.components.EntryToolComponent;

/**
 * ToolTrackerItem
 */
public class ToolTrackerItem extends Item implements TrackerItem {//} implements ItemComponentCallback {

    public ToolTrackerItem() {
        super(new Settings().group(ModInit.toolTrackerGroup).maxCount(16));
    }

    @Override
    public void appendTooltip(ItemStack stack, World world, List<Text> tooltip, TooltipContext context) {
        EntryToolComponent.KEY.borrowPooledComponent(stack, c->{
            Block b = c.get();
            if(b != null && b != Blocks.AIR) {
                tooltip.add(new TranslatableText(c.get().getTranslationKey()).formatted(Formatting.GRAY));
            }
        }, false);
    }
}