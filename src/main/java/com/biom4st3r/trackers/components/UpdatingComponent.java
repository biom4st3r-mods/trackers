package com.biom4st3r.trackers.components;

import net.minecraft.stat.Stat;

public abstract class UpdatingComponent extends TrackerComponent {
    static {
        PooledItemComponent.registerSubType(UpdatingComponent.class);
    }

    /**
     * returns wheather or not the object was modified and should be reserialized
     * @param stat
     * @param i
     * @return
     */
    public abstract boolean react(Stat<?> stat, int i);
}
