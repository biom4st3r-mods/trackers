package com.biom4st3r.trackers.components;

public abstract class ItemSpecificComponent extends PooledItemComponent {
    static {
        PooledItemComponent.registerSubType(ItemSpecificComponent.class);
    }
}
