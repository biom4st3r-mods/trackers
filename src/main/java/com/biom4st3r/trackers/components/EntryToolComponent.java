package com.biom4st3r.trackers.components;

import net.minecraft.block.Block;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;

import com.biom4st3r.trackers.ModInit;

public class EntryToolComponent extends ItemSpecificComponent {

    public static final ComponentKey<EntryToolComponent> KEY = ComponentKey.of(EntryToolComponent.class, "tracker:entrytool", EntryToolComponent::new);

    @Override
    public ComponentKey<? extends PooledItemComponent> getKey() {
        return KEY;
    }

    Block block = null;

    @Override
    public void fromStack(ItemStack stack) {
        block = Registry.BLOCK.get(
            new Identifier(
                stack.getOrCreateSubTag(KEY.id).getString("blockid")
            )
        );
    }

    public Block get() {
        return block;
    }
    public void set(Block type) {
        this.block = type;
    }

    @Override
    public void toStack(ItemStack stack) {
        if(block == null) return;
        stack.getOrCreateSubTag(KEY.id).putString("blockid", Registry.BLOCK.getId(block).toString());
    }

    @Override
    public void clear() {
        block = null;
    }

    @Override
    public boolean validItem(ItemStack i) {
        return i.getItem() == ModInit.Tool_Tracker_Item;
    }
}
