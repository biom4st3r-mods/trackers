package com.biom4st3r.trackers.components;

import java.util.Collections;
import java.util.List;

import net.minecraft.entity.EntityType;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.nbt.NbtList;
import net.minecraft.stat.Stat;
import net.minecraft.stat.Stats;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.Formatting;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;

import com.google.common.collect.Lists;
import it.unimi.dsi.fastutil.objects.Object2IntMap;
import it.unimi.dsi.fastutil.objects.Object2IntMap.Entry;
import it.unimi.dsi.fastutil.objects.Object2IntOpenHashMap;

public class KillTrackerComponent extends UpdatingComponent {

    public static final ComponentKey<KillTrackerComponent> KEY = ComponentKey.of(KillTrackerComponent.class, "tracker:killtracker", KillTrackerComponent::new);

    Object2IntMap<EntityType<?>> trackerMap = new Object2IntOpenHashMap<>();
    
    @Override
    public ComponentKey<? extends PooledItemComponent> getKey() {
        return KEY;
    }

    @Override
    public void fromStack(ItemStack stack) {
        NbtList lt = (NbtList) stack.getOrCreateSubTag(KEY.id).getList("list", 10);
        lt.forEach(ct-> {
            trackerMap.put(
                Registry.ENTITY_TYPE.get(
                    new Identifier( ((NbtCompound) ct).getString("k")) ), 
                    ((NbtCompound) ct).getInt("v")
            );
        });
    }

    public void add(EntryKillComponent component) {
        trackerMap.computeIfAbsent(component.type, t->0);
    }

    @Override
    public void toStack(ItemStack stack) {
        if(trackerMap.isEmpty()) return;
        NbtCompound tag = stack.getOrCreateSubTag(KEY.id);
        NbtList lt = tag.getList("list", 10);
        lt.clear();

        trackerMap.forEach((type,val)-> {
            NbtCompound ct = new NbtCompound();
            ct.putString("k", EntityType.getId(type).toString());
            ct.putInt("v", val);
            lt.add(ct);
        });
        tag.put("list", lt);
        // stack.getTag().put(KEY.id, tag);
    }

    @Override
    public List<Text> getToolTips() {
        if(trackerMap.isEmpty()) return Collections.emptyList();
        List<Text> list = Lists.newArrayList();
        for(Entry<EntityType<?>> x : trackerMap.object2IntEntrySet()) {
            
            list.add(new TranslatableText("dialog.itemtrackers.killtracker", x.getIntValue(), new TranslatableText(x.getKey().getTranslationKey())).formatted(Formatting.DARK_GRAY));
        }
        return list;
    }

    @Override
    public boolean react(Stat<?> stat, int i) {
        if(stat.getType() == Stats.KILLED && trackerMap.containsKey(stat.getValue())) {
            trackerMap.put((EntityType<?>) stat.getValue(), trackerMap.getInt(stat.getValue())+1);
            return true;
        }
        return false;
    }

    @Override
    public void clear() {
        trackerMap.clear();
    }
    
}
