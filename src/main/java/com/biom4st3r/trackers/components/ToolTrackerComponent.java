package com.biom4st3r.trackers.components;

import java.util.Collections;
import java.util.List;

import net.minecraft.block.Block;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.nbt.NbtList;
import net.minecraft.stat.Stat;
import net.minecraft.stat.Stats;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.Formatting;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;

import com.google.common.collect.Lists;
import it.unimi.dsi.fastutil.objects.Object2IntArrayMap;
import it.unimi.dsi.fastutil.objects.Object2IntMap;
import it.unimi.dsi.fastutil.objects.Object2IntMap.Entry;

public class ToolTrackerComponent extends UpdatingComponent {
    // static {
    //     PooledItemComponent.register(ToolTrackerComponent.class, ToolTrackerComponent::new);
    // }
    public static final ComponentKey<ToolTrackerComponent> KEY = ComponentKey.of(ToolTrackerComponent.class, "tracker:tooltracker", ToolTrackerComponent::new);

    Object2IntMap<Block> trackerMap = new Object2IntArrayMap<>();

    @Override
    public ComponentKey<? extends PooledItemComponent> getKey() {
        return KEY;
    }

    public void add(EntryToolComponent c) {
        this.trackerMap.computeIfAbsent(c.get(), b->0);
    }

    @Override
    public void fromStack(ItemStack stack) {
        NbtList lt = stack.getOrCreateSubTag(KEY.id).getList("list", 10);
        lt.forEach(tag-> {
            trackerMap.put(
                Registry.BLOCK.get(new Identifier(((NbtCompound) tag).getString("k"))), 
                ((NbtCompound) tag).getInt("v"));
        });
    }

    @Override
    public void toStack(ItemStack stack) {
        if(trackerMap.isEmpty()) return;
        NbtCompound tag = stack.getOrCreateSubTag(KEY.id);
        NbtList lt = tag.getList("list", 10);
        trackerMap.forEach((block,val)-> {
            NbtCompound ct = new NbtCompound();
            ct.putString("k", Registry.BLOCK.getId(block).toString());
            ct.putInt("v", val);
            lt.add(ct);
        });
        tag.put("list", lt);
    }

    @Override
    public List<Text> getToolTips() {
        if(trackerMap.isEmpty()) return Collections.emptyList();
        List<Text> list = Lists.newArrayList();
        for(Entry<Block> x : trackerMap.object2IntEntrySet()) {
            
            list.add(new TranslatableText("dialog.itemtrackers.tooltracker", x.getIntValue(), new TranslatableText(x.getKey().getTranslationKey())).formatted(Formatting.DARK_GRAY));
        }
        return list;
    }

    @Override
    public boolean react(Stat<?> stat, int i) {
        if(stat.getType() == Stats.MINED && trackerMap.containsKey(stat.getValue())) {
            trackerMap.put((Block) stat.getValue(), trackerMap.getInt(stat.getValue())+1);
            return true;
        }
        return false;
    }

    @Override
    public void clear() {
        trackerMap.clear();
    }
    
}
