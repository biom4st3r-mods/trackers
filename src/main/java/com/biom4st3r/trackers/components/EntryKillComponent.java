package com.biom4st3r.trackers.components;

import net.minecraft.entity.EntityType;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;

import com.biom4st3r.trackers.ModInit;

public class EntryKillComponent extends ItemSpecificComponent {
    public static final ComponentKey<EntryKillComponent> KEY = ComponentKey.of(EntryKillComponent.class, "tracker:entrykill", EntryKillComponent::new);

    EntityType<?> type = null;

    public EntityType<?> get() {
        return type;
    }
    
    @Override
    public ComponentKey<? extends PooledItemComponent> getKey() {
        return KEY;
    }

    public void set(EntityType<?> type) {
        this.type = type;
    }

    @Override
    public void fromStack(ItemStack stack) {
        type = Registry.ENTITY_TYPE.get(
            new Identifier(
                stack.getOrCreateSubTag(KEY.id).getString("entityid")
            )
        );
    }

    @Override
    public void toStack(ItemStack stack) {
        if(type == null) return;
        stack.getOrCreateSubTag(KEY.id).putString("entityid", Registry.ENTITY_TYPE.getId(type).toString());
    }

    @Override
    public void clear() {
        type = null;
    }

    @Override
    public boolean validItem(ItemStack i) {
        return i.getItem() == ModInit.Kill_Tracker_Item;
    }
}
