package com.biom4st3r.trackers.components;

import java.util.Collections;
import java.util.Date;
import java.util.List;

import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableText;

public class CraftedComponent extends TrackerComponent {
    public static final ComponentKey<CraftedComponent> KEY = ComponentKey.of(CraftedComponent.class, "tracker:craftedcomp", CraftedComponent::new);

    private String owner = "";
    private long time;

    public boolean hasOwner() {
        return !owner.isEmpty();
    } 

    public void setOwner(String newOwner) {
        if(this.owner.isBlank()) {
            this.time = new Date().getTime();
            this.owner = newOwner;
        }
    }

    @Override
    public void fromStack(ItemStack stack) {
        NbtCompound ct = stack.getOrCreateSubTag(KEY.id);
        owner = ct.getString("owner");
        time = ct.getLong("time");
    }
    
    @Override
    public ComponentKey<? extends PooledItemComponent> getKey() {
        return KEY;
    }

    @Override
    public void toStack(ItemStack stack) {
        if(owner.isEmpty()) return; // Early return.

        NbtCompound ct = stack.getOrCreateSubTag(KEY.id);
        ct.putString("owner", owner);
        ct.putLong("time", time);
    }

    @Override
    public List<Text> getToolTips() {
        if(this.owner.isEmpty()) return Collections.emptyList();
        return Collections.singletonList(new TranslatableText("dialog.itemtrackers.owner", owner));
    }

    @Override
    public void clear() {
        owner = "";
        time = -1;
    }
}
