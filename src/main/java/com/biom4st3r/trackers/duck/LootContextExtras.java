package com.biom4st3r.trackers.duck;

import net.minecraft.loot.context.LootContextType;

public interface LootContextExtras {
    LootContextType getType();
    void setType(LootContextType type);
}
