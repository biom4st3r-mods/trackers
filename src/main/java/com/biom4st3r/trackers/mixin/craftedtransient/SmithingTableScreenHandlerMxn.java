package com.biom4st3r.trackers.mixin.craftedtransient;

import java.util.List;

import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.recipe.SmithingRecipe;
import net.minecraft.screen.ForgingScreenHandler;
import net.minecraft.screen.ScreenHandlerContext;
import net.minecraft.screen.ScreenHandlerType;
import net.minecraft.screen.SmithingScreenHandler;

import com.biom4st3r.trackers.components.PooledItemComponent;
import com.biom4st3r.trackers.components.TrackerComponent;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.At.Shift;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.LocalCapture;

@Mixin(SmithingScreenHandler.class)
public abstract class SmithingTableScreenHandlerMxn extends ForgingScreenHandler {

    public SmithingTableScreenHandlerMxn(ScreenHandlerType<?> type, int syncId, PlayerInventory playerInventory,
            ScreenHandlerContext context) {
        super(type, syncId, playerInventory, context);
    }
    
    /**
     * Collect all ITrackerComponents from input items and output items and getPreferedComponenets from all to be added to output
     */
    @Inject(
       at = @At(
          value="INVOKE",
          target="net/minecraft/inventory/CraftingResultInventory.setLastRecipe(Lnet/minecraft/recipe/Recipe;)V",
          ordinal = -1,
          shift = Shift.AFTER),
       method = "updateResult",
       cancellable = false,
       locals = LocalCapture.CAPTURE_FAILHARD)
    private void updateResultWithComponenets(CallbackInfo ci, List<SmithingRecipe> list, ItemStack outputStack)
    {
        if(!this.player.world.isClient)
        {
            PooledItemComponent.borrowAllComponentsOfSubType(TrackerComponent.class, this.input.getStack(0), l->l.forEach(c->c.toStack(outputStack)), false);
        }
    }

}