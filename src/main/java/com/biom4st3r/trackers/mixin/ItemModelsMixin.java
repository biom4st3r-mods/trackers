package com.biom4st3r.trackers.mixin;

import com.biom4st3r.trackers.OnItemModelReload;

import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import it.unimi.dsi.fastutil.ints.Int2ObjectMap;
import net.minecraft.client.render.item.ItemModels;
import net.minecraft.client.render.model.BakedModel;

/**
 * ItemModels
 * Inject models for my items directly into the Model list
 */
@Mixin(ItemModels.class)
public abstract class ItemModelsMixin {

    @Shadow @Final
    private Int2ObjectMap<BakedModel> models;

    @Inject(method="reloadModels",at = @At("TAIL"))
    public void injectItemModels(CallbackInfo ci)
    {
        OnItemModelReload.EVENT.invoker().onReload((ItemModels)(Object)this);
    }

    
}