package com.biom4st3r.trackers;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import net.minecraft.entity.EntityType;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.loot.condition.RandomChanceLootCondition;
import net.minecraft.loot.provider.number.ConstantLootNumberProvider;
import net.minecraft.recipe.RecipeType;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;

import net.fabricmc.api.ClientModInitializer;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.client.itemgroup.FabricItemGroupBuilder;
import net.fabricmc.fabric.api.loot.v1.FabricLootPoolBuilder;
import net.fabricmc.fabric.api.loot.v1.event.LootTableLoadingCallback;

import com.biom4st3r.coderecipes.api.RecipeHelper;
import com.biom4st3r.coderecipes.api.Recipes;
import com.biom4st3r.trackers.components.CraftedComponent;
import com.biom4st3r.trackers.components.DamageTrackingComponent;
import com.biom4st3r.trackers.components.EntryKillComponent;
import com.biom4st3r.trackers.components.EntryToolComponent;
import com.biom4st3r.trackers.components.FoundLootComponent;
import com.biom4st3r.trackers.components.KillTrackerComponent;
import com.biom4st3r.trackers.components.ToolTrackerComponent;
import com.biom4st3r.trackers.items.KillTrackerItem;
import com.biom4st3r.trackers.items.ToolTrackerItem;
import io.github.cottonmc.cotton.config.ConfigManager;

public class ModInit implements ModInitializer, ClientModInitializer {

    public static final String MODID = "itemtrackers";

    private static final Config config = ConfigManager.loadConfig(Config.class);

    public static List<Item> itemsWithKillTracker = Stream.of(config.supportsKillTrackerComponent).map(Identifier::new).map(Registry.ITEM::get).collect(Collectors.toList());
    public static List<Item> itemsWithToolTracker = Stream.of(config.supportsToolTrackerComponent).map(Identifier::new).map(Registry.ITEM::get).collect(Collectors.toList());

    public static final Item Tool_Tracker_Item = Registry.register(Registry.ITEM,
    new Identifier(ModInit.MODID, "tooltrackeritem"), new ToolTrackerItem());
    public static final Item Kill_Tracker_Item = Registry.register(Registry.ITEM, 
    new Identifier(ModInit.MODID, "killtrackeritem"), new KillTrackerItem());
    public static final ItemGroup killTrackersGroup = FabricItemGroupBuilder
        .create(new Identifier(MODID, "killtrackertab"))
        .icon(() -> new ItemStack(Items.PAPER))
        .appendItems(list-> {
            Stream
                .of(config.supportsBeingKillTrackedEntities)
                .map(Identifier::new)
                .map(Registry.ENTITY_TYPE::get)
                .map(et-> {
                    ItemStack is = new ItemStack(Kill_Tracker_Item);
                    EntryKillComponent.KEY.borrowPooledComponent(is, c->c.set(et), true);
                    return is;
                })
                .forEach(list::add);
        })
        .build();
    public static final ItemGroup toolTrackerGroup = FabricItemGroupBuilder
        .create(new Identifier(MODID, "tooltrackertab"))
        .icon(() -> new ItemStack(Items.PAPER))
        .appendItems(list-> {
            Registry.ITEM.stream().filter(BlockItem.class::isInstance)
                .map(item-> {
                    ItemStack is = new ItemStack(Tool_Tracker_Item);
                    EntryToolComponent.KEY.borrowPooledComponent(is, c->c.set(((BlockItem)item).getBlock()), true);
                    return is;
                })
                .forEach(list::add);
        })
        .build();


    @Override
    @Environment(EnvType.CLIENT)
    public void onInitializeClient() {
        OnItemModelReload.EVENT.register((models) -> {
            models.models.put(Registry.ITEM.getRawId(Tool_Tracker_Item), TrackerItemModel.INSTANCE);
            models.models.put(Registry.ITEM.getRawId(Kill_Tracker_Item), TrackerItemModel.INSTANCE);
        });
    }

    @Override
    public void onInitialize() {
        CraftedComponent.KEY.noop();
        FoundLootComponent.KEY.noop();
        EntryKillComponent.KEY.noop();
        EntryToolComponent.KEY.noop();
        KillTrackerComponent.KEY.noop();
        ToolTrackerComponent.KEY.noop();
        DamageTrackingComponent.KEY.noop();

		Set<Identifier> ids = Stream.of(config.supportsBeingKillTrackedEntities)
			.map(s->s.split(":"))
			.map(sa->new Identifier(sa[0], "entities/" + sa[1]))
			.collect(Collectors.toSet());
        //data merge block 138 69 78 {Items:[{Slot:1b, id:"itemtrackers:tooltrackeritem", Count:1b, tag:{"tracker:entrytool":{blockid:"minecraft:dirt"}}}]}

		initRecipe();

        // Droptable for each entityType that has an item
        LootTableLoadingCallback.EVENT.register((resourceManager, lootManager, id, supplier, setter) -> {
            if (ids.contains(id)) {
                EntityType<?> et = Registry.ENTITY_TYPE.get(new Identifier(id.toString().replace("entities/", "")));
                ItemStack is = new ItemStack(Kill_Tracker_Item);
				EntryKillComponent.KEY.borrowPooledComponent(is, c->c.set(et), true);
                
                supplier.withPool(
                    FabricLootPoolBuilder
                    .builder()
                    .rolls(ConstantLootNumberProvider.create(1F))
                    .with(StackEntry.builder(is).weight(1).conditionally(RandomChanceLootCondition.builder(0.01F)))
                    .build()
                ).build();
            }
        });
    }

    public static <A> boolean contains(A[] array, A string) {
        for (int i = 0; i < array.length; i++)
            if (array[i] == string)
                return true;
        return false;
    }

	public static void initRecipe() {
        Identifier id = new Identifier(ModInit.MODID, "tooltrackerrecipe");
        Item[] blockItems = Registry.ITEM.stream().filter((item) -> item instanceof BlockItem).toArray(Item[]::new);
        RecipeHelper.register(id, RecipeType.CRAFTING,
        Recipes.newRefShapelessRecipe()
            .addIngredient(blockItems)
            .addIngredient(Items.PAPER)
            .build(id,"nogroup", new ItemStack(ModInit.Tool_Tracker_Item), (recipe, list, player) -> {
                Item i0 = null;
                for(ItemStack is : list) if(ModInit.contains(blockItems, is.getItem())) i0 = is.getItem();
                Item item = i0;
                ItemStack output = recipe.getOutput().copy();
                EntryToolComponent.KEY.borrowPooledComponent(output, c->c.set(((BlockItem)item).getBlock()), true);
                return output;
            }));
        id = new Identifier(ModInit.MODID, "tooltrackblockappender");
        RecipeHelper.register(id, RecipeType.CRAFTING,
        Recipes.newRefShapelessRecipe()
            .addIngredient(ModInit.itemsWithToolTracker.toArray(Item[]::new))
            .addIngredient(ModInit.Tool_Tracker_Item)
            .build(id, "nogroup", new ItemStack(Items.PAPER), (recipe, list, player) -> {
                ItemStack tracker0 = null;
                ItemStack tool = null;
                for(ItemStack is : list) if(is.getItem() == ModInit.Tool_Tracker_Item) tracker0 = is;
                for(ItemStack is : list) if(is.getItem() != ModInit.Tool_Tracker_Item && !is.isEmpty()) tool = is;
                ItemStack tracker = tracker0;
                
                ToolTrackerComponent.KEY.borrowPooledComponent(tool, c -> {
                    EntryToolComponent entry = EntryToolComponent.KEY.getComponent(tracker).get();
                    c.add(entry);
                    entry.release(false);
                }, true);
                return tool;
        }));
        id = new Identifier(ModInit.MODID, "killtrackerrecipe");
        RecipeHelper.register(id, RecipeType.CRAFTING,
        Recipes.newRefShapelessRecipe()
            .addIngredient(ModInit.itemsWithKillTracker.toArray(Item[]::new))
            .addIngredient(ModInit.Kill_Tracker_Item)
            .build(id, "nogroup", new ItemStack(Items.PAPER), (recipe, stacks, player) -> {
                ItemStack tool = null;
                for (ItemStack is : stacks) {
					if(Kill_Tracker_Item != is.getItem()) tool = is;
				}
				KillTrackerComponent.KEY.borrowPooledComponent(tool, c-> {
					ItemStack tracker = null;
					for(ItemStack is : stacks) {
						if(Kill_Tracker_Item == is.getItem()) tracker = is;
					}
					EntryKillComponent entry = EntryKillComponent.KEY.getComponent(tracker).get();
					c.add(entry);
					entry.release(false);
				}, true);
                return tool.copy();
            }));
    }
    // private static final BioLogger logger = new BioLogger("TrackerInit");
}
